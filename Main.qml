import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Window 2.11
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    visible: true
    width: 900
    height: 700
    title: "Ales Pejznoch IFS editor"
    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }
    
    menuBar: MenuBar {
        contentHeight: 30
        implicitHeight: 30
        Menu {
            title: "&Iterator"
            Action { text: "&Open iterator" }
            Action { text: "&Save Iterator" }
            Action { text: "&Export into TGA" }
            MenuSeparator { }
            Action { text: "&Quit" }
        }
        Menu {
            title: "&Help"
            Action { text: "Iterator &format" }
            Action { text: "&About" }
        }
    }
    
    Rectangle {
        width: 230
        height: parent.height
        color: myPalette.alternateBase
        anchors.right: parent.right
        
        Column {
            id: maincolumn
            anchors.fill: parent
//            spacing: ((root.height)-(group1.height+group2.height+group3.height))/3
            spacing: 10
            padding: 10
            
            GroupBox {
                id: group1
                title: "Iterator"
                anchors.margins: parent.padding
                anchors.left: parent.left
                anchors.right: parent.right
                Column {
                    id: group1col
                    anchors.fill: parent
                    padding: 6
                    spacing: 6
                    Text {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        text: "Primary iterator type:"
                    }
                    ComboBox {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                    Text {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        text: "Color drawing method:"
                    }
                    ComboBox {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                    Row {
                        spacing: 6
                        Item {
                            width: group1col.width/2 - group1col.padding - parent.spacing/2
                            height: cg.height
                            Rectangle {
                               anchors.margins: 10 
                                anchors.fill: parent
                                color: "#ffffff"
                                MouseArea {
                                }
                            }
                        }
                        Button {
                            id: cg
                            text: "Draw"
                            highlighted: true
                            width: group1col.width/2 - group1col.padding - parent.spacing/2
                            height: 50
                        }
                    }
                }
            }
            
            
            GroupBox {
                id: group2
                title: "Morfing"
                anchors.margins: parent.padding
                anchors.left: parent.left
                anchors.right: parent.right
                Column {
                    id: group2col
                    anchors.fill: parent
                    padding: 6
                    spacing: 6
                    Text {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        text: "Secondary iterator type:"
                    }
                    ComboBox {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                    Row {
                        spacing: 6
                        SpinBox {
                            from: 1
                            to: 99
                            stepSize: 1
                            value: 40
                            editable: true
                            textFromValue: function(value, locale) {
                                              return (value === 1 ? qsTr("%1 %")
                                                                  : qsTr("%1 %")).arg(value);
                                           }
                            width: group2col.width/2 - group2col.padding - parent.spacing/2
                        }
                        Button {
                            text: "Morph"
                            width: group2col.width/2 - group2col.padding - parent.spacing/2
                        }
                    }
                    Button {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: "Animate"
                    }
                }
            }
            
            
            GroupBox {
                id: group3
                title: "View"
                anchors.margins: parent.padding
                anchors.left: parent.left
                anchors.right: parent.right
                Column {
                    anchors.fill: parent
                    padding: 5
                    spacing: 5
                    RowLayout {
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
//                        anchors.fill: parent
//                        Layout.fillWidth: parent
//                        Layout.minimumWidth: 10
//                        implicitWidth: 40
                        Button {
                               Layout.fillHeight: true
                               Layout.fillWidth: true
                               text: "\u2191"
                        }
                        Button {
                               Layout.fillHeight: true
                               Layout.fillWidth: true
                               text: "\u2193"
                        }
                        Button {
                               Layout.fillHeight: true
                               Layout.fillWidth: true
                               text: "\u2190"
                        }
                        Button {
                               Layout.fillHeight: true
                               Layout.fillWidth: true
                               text: "\u2192"
                        }
                    }
                    RowLayout {
                        
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                        ColumnLayout {
                            Button {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                text: "+"
                            }
                            Button {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                text: "Iter++"
                            }
                        }
                        ColumnLayout {
                            Button {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                text: "-"
                            }
                            Button {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                text: "iter--"
                            }
                        }
                    }
                    Button {
                        text: "Restore view"
                        anchors.margins: parent.padding
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                }
            }
        }
    }
}
