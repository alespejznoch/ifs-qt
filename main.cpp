#include <QIcon>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

 int main(int argc, char *argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setApplicationName("GUI");
    QGuiApplication app(argc, argv);
    
    QQuickStyle::setStyle("Fusion");
    
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    QIcon icon(":/IFS.ico");
    app.setWindowIcon(icon);
    return app.exec();
}
