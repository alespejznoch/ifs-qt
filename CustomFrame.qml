import QtQuick 2.10
import QtQuick.Controls 2.3

GroupBox {
    id: root
//    property alias title: root.title
    Column {
        anchors.fill: parent
        CheckBox { text: qsTr("E-mail") }
        CheckBox { text: qsTr("Calendar") }
        CheckBox { text: qsTr("Contacts") }
    }
}
